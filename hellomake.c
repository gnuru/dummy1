/*
 * from http://www.cs.colby.edu/maxwell/courses/tutorials/maketutor/
 */
#include <hellomake.h>

int main() {
  // call a function in another file
  myPrintHelloMake();

  return(0);
}
